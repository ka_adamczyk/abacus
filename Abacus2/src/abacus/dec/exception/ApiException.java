package abacus.dec.exception;

import java.io.IOException;

public class ApiException extends Exception {
	private static final long serialVersionUID = 130603610135751033L;

	public ApiException(String msg) {
		super(msg);
	}

	public ApiException(String msg, IOException e) {
		super(msg, e);
	}
}
