package abacus.dec.dao;

import java.util.Random;

public class MathDao {

	public Integer getRandomInteger() {
		Random random = new Random();
		return (random.nextInt(2 * 1000) - 1000);
	}

	public Integer getRandomInteger(int min, int max) {
		Random random = new Random();
		return (random.nextInt(2 * max) - min);
	}
}
