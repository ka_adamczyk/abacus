package abacus.dec.util;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ProcessResult {

	private final int statusCode;

	private final String body;

	private final Map<String, List<String>> headers;

	public ProcessResult(int statusCode, String body) {
		this(statusCode, body, new HashMap<String, List<String>>());
	}

	public ProcessResult(int statusCode, String body, Map<String, List<String>> headers) {
		this.statusCode = statusCode;
		this.body = body;
		this.headers = headers;
	}

	public int getStatusCode() {
		return statusCode;
	}

	public String getBody() {
		return body;
	}

	public Map<String, List<String>> getHeaders() {
		return headers;
	}

	@Override
	public String toString() {
		return "ProcessResult [statusCode=" + statusCode + ", body=" + body + "]";
	}
}