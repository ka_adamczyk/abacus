package abacus.dec.validator;

public class ValidatorUtil {

	public static Integer validateInteger(String value) {
		try {
			return Integer.valueOf(value);
		} catch (NumberFormatException e) {
			throw new IllegalArgumentException("Wrong value: " + value);
		}
	}
}
