package abacus.dec;

import abacus.dec.dao.MathDao;
import abacus.dec.exception.ApiException;
import abacus.dec.service.Calculator;
import abacus.dec.service.RandomClient;
import abacus.dec.util.ProcessResult;
import abacus.dec.validator.ValidatorUtil;

public class App {

	public static void main(String[] args) {

		try {
			RandomClient client = new RandomClient();
			ProcessResult response = client.getSingleInteger();
			client.validateResponse(response);

			Integer first = ValidatorUtil.validateInteger(response.getBody().trim());
			System.out.println("First value: " + first);

			MathDao dao = new MathDao();
			Integer second = dao.getRandomInteger();
			System.out.println("Second value: " + second);

			Calculator calc = new Calculator();
			Integer result = calc.add(first, second);
			System.out.println(result);

		} catch (ApiException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}
