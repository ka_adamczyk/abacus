package abacus.dec.service;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;

import org.apache.commons.io.IOUtils;

import abacus.dec.exception.ApiException;
import abacus.dec.util.ProcessResult;

public class RandomClient {

	private static final String RANDOM_ORG_URL = "https://www.random.org/integers?num=1&col=1&base=10&format=plain&rnd=new";

	private static final String USER_AGENT = "Mozilla/5.0";

	private int timeout = 60000;

	public ProcessResult getSingleInteger() throws ApiException {
		return getSingleInteger(-1000, 1000);
	}

	public ProcessResult getSingleInteger(int min, int max) throws ApiException {
		if (min > max) {
			throw new IllegalArgumentException("Min value must be lower then max value");
		}

		String urlWithParams = RANDOM_ORG_URL + "&min=" + min + "&max=" + max;
		HttpURLConnection connection = null;
		ProcessResult result = null;

		try {
			URL url = new URL(urlWithParams);
			connection = (HttpURLConnection) url.openConnection();
			connection.setRequestMethod("GET");

			connection.setRequestProperty("User-Agent", USER_AGENT);

			connection.setUseCaches(false);
			// true if we want to read server's response
			connection.setDoInput(true);
			// false indicates this is a GET request
			connection.setDoOutput(false);
			connection.setConnectTimeout(timeout);

			int responseCode = connection.getResponseCode();
			InputStream responseInputStream = connection.getInputStream();
			String body = IOUtils.toString(responseInputStream, StandardCharsets.UTF_8);
			result = new ProcessResult(responseCode, body, connection.getHeaderFields());

		} catch (IOException e) {
			throw new ApiException("getSingleInteger GET request not worked", e);

		} finally {
			if (connection != null) {
				connection.disconnect();
			}
		}

		return result;
	}

	public void validateResponse(ProcessResult response) throws ApiException {
		if (response.getStatusCode() != HttpURLConnection.HTTP_OK) {
			throw new ApiException("validateResponse GET request not worked, response: " + response);
		}
	}

}
